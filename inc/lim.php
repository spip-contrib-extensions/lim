<?php
/**
 * Fonctions utiles au plugin Lim
 *
 * @plugin     Lim
 * @copyright  2015
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\Lim\Inc
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérifier si il existe déjà des logos de téléchargés pour un type d'objet
 * Exception : le logo du site (dans 'Identité du site') n'est pas pris en compte
 * 
 * @table_sql string (ex. : spip_articles)
 *
 * @return bool
 */
function lim_verifier_presence_logo($table_sql) {
	include_spip('base/objets');
	$objet = objet_type($table_sql);

	switch ($objet) {
		case 'spip_syndic': // gérer le cas du logo du site : ne pas le prendre en compte ici (logo du site -> id_objet = 0).
			$where = "L.objet=".sql_quote($objet)." AND L.id_objet!=0 AND D.mode=".sql_quote('logoon');
			break;
		default:
			$where = "L.objet=".sql_quote($objet).' AND D.mode='.sql_quote('logoon');
			break;
	}

	$existe_logos = sql_getfetsel('D.id_document', 'spip_documents AS D JOIN spip_documents_liens AS L ON D.id_document=L.id_document', $where);
	
	if ($existe_logos){
		return true;
	}

	return false;
}

/**
 * Vérifier si il existe déjà des objets dans la rubrique
 *
 * @param int $id_rubrique
 * @param string $type de l'objet
 * @return bool
 */
function lim_verifier_presence_objets($id_rubrique, $type) {
	$table = table_objet_sql($type);
	if (sql_countsel($table, "id_rubrique=$id_rubrique") > 0) return true;
	return false;
}


/**
 * Construire la liste des objets à exclure
 * les objets SPIP qui ne sont jamais listés dans rubrique, et donc non pertinents dans la restriction par rubrique.
 *
 * exception : pour les brèves et les sites, on vérifie qu'elles ont été activées
 * exception : les documents si ceux-ci ont été activés dans les rubriques (menu Configuration -> Contenu du site -> paragraphe Documents joints)
 * 
 * @return array
 *	tableau des nom de tables SPIP à exclure (ex : spip_auteurs, spip_mots, etc.)
 */
function lim_objets_a_exclure() {
	$exclus = array();
	$tables = lister_tables_objets_sql();
	foreach ($tables as $key => $value) {
		if ($value['editable'] == 'oui' AND !isset($value['field']['id_rubrique']))
			array_push($exclus,$key);	
	}
	
	// Exception pour les objets breves et sites : sont-ils activés
	if (lire_config('activer_breves') == 'non') {
		array_push($exclus, 'spip_breves');
	}
	if (lire_config('activer_sites') == 'non') {
		array_push($exclus, 'spip_syndic');
	}

	// Exception pour les documents (si ils ont été activés pour les rubriques)
	$document_objet = lire_config('documents_objets');
	if (strpos($document_objet, 'spip_rubriques')) {
		$key = array_search('spip_documents', $exclus);
		unset($exclus[$key]);
	}

	// donner aux plugins la possibilité de gérer les exclusions (ajouter, supprimer une exclusion)
	$exclus = pipeline('lim_declare_exclus', $exclus);

	return $exclus;
}

/**
 * Récupérer la liste des rubriques dans lesquelles il est possible de créer l'objet demandé
 * 
 * @param string $type de l'objet
 * @return array
 */

function lim_publierdansrubriques($type) {
	$rubriques_autorisees = array();

	$tab_rubriques_exclues = lire_config("lim_rubriques/$type"); // renvoi NULL si meta/lim_rubriques/$type n'existe pas

	if ($tab_rubriques_exclues) {
		$res = sql_allfetsel('id_rubrique', 'spip_rubriques');
		$tab_rubriques = array_column($res, 'id_rubrique');
		$rubriques_autorisees = array_diff($tab_rubriques, $tab_rubriques_exclues);
	} 

	return $rubriques_autorisees;
}

/**
 * Retourner le nombre de rubriques dans lesquelles il est possible de créer l'objet demandé
 * 
 * @param string $type de l'objet
 * @return int
 */

function lim_nbre_rubriques_autorisees($type) {
	// par défaut c'est le nombre total de rubrique
	$nbre_rubriques_autorisees = sql_countsel('spip_rubriques');

	$tab_rubriques_exclues	= lire_config("lim_rubriques/$type");
	if ($tab_rubriques_exclues) {
		$nbre_rubriques_autorisees = $nbre_rubriques_autorisees - count($tab_rubriques_exclues);
	}

	return $nbre_rubriques_autorisees;
}

/**
 * Gestion des contenus par rubriques : renvoyer le tableau des objets sélectionnés au format array($type_objet, trad(texte_objets))
 * Ex. en français : array('article' => 'Articles', 'site' => 'Sites référencés')
 * Ex. en espagnol : array('article' => 'Artículos', 'site' => 'Sitios referenciados')
 * 
 * @return array
 */

function lim_get_rubriques_objets() {
	include_spip('inc/config');
	include_spip('base/objets');

	$liste = array();
	if ($objets_lim = lire_config('lim/rubriques/objets')) {
		$tables_spip = lister_tables_spip();
		foreach ($objets_lim as $value) {
			if(!empty($value)) {
				$table_objet = table_objet($value);
				$key = objet_type($value);
				$infos_table = lister_tables_objets_sql($value);
				if (in_array($table_objet, $tables_spip)) {
					$liste[$key] = _T($infos_table['texte_objets']);
				}
			}
		}
	}
	
	return $liste;
}


function lim_valeur_cadenas($objet) {
	include_spip('inc/config');

	$cadenas = null;

	$table = table_objet_sql($objet);
	if ($objets_cadenas = lire_config('lim/rubriques/cadenas')) {
		in_array($table, $objets_cadenas) ? $cadenas = true : $cadenas = false;
	}

	return $cadenas;
}
