<?php
/**
 * Utilisations de pipelines par Lim
 *
 * @plugin     Lim
 * @copyright  2015
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\Lim\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/lim');
include_spip('inc/config');

/**
 * gestion forums publics : supprime ou non le bloc en fonction de la demande
 *
 * @param array $flux
 * @return array $flux
 *     le flux data remanié
**/
function lim_afficher_config_objet($flux) {
	$type = $flux['args']['type'];
	if ($type == 'article' AND !empty($flux['data'])) {

		$tab_data = explode("<div class='ajax'>", $flux['data']);
		$tab_data[1] = "<div class='ajax'>".$tab_data[1];

		if ( strpos($tab_data[1], 'formulaire_activer_forums') AND lire_config('forums_publics') == 'non' AND lire_config('lim/divers/forums_publics') == 'on' ) {
			$tab_data[1] = '';
		}

		$flux['data'] = $tab_data[1];
	}
	return $flux;
}

/**
 * Gestion des contenus par rubrique
 * Lors de la création d'un objet éditorial, être sûr de renvoyer la bonne valeur de "id_parent".
 *
 * Cas #1 : objet géré par la restriction par rubrique de LIM
 * 	->	#1.1 Si une seule rubrique définie : on récupère la valeur de l'id_rubrique dans la conf.
 * 	->	#1.2 Sinon forcer la valeur vide pour 'id_parent'. Dans ce cas, le premier choix du selecteur de rubrique est vide, et si l'utilisateur ne choisit pas de rubrique il aura un retour en erreur sur un champ obligatoire (sauf dans le cas d'une rubrique qui se retrouvera à la racine).
 *
 * Cas #2 : objet non géré par la restriction par rubrique de LIM.
 * -> Vérifier que l'objet a comme parent une rubrique et si oui, même traitement que pour le cas #1.2
 *
 * @param array $flux
 * @return array $flux
 *     le flux data complété par une valeur de id_parent
**/
function lim_formulaire_charger($flux) {
	if (
		strncmp($flux['args']['form'], 'editer_', 7) == 0 // c'est bien un formulaire d'edition d'objet
		and isset($flux['args']['args']['0'])
		and !is_numeric($flux['args']['args']['0']) // c'est bien une création d'objet (pas une modif ou autre)
	) {
		if (!empty($flux['data']['id_parent'])) {
			$id_parent = $flux['data']['id_parent'];
		}
		else {
			$objet = substr($flux['args']['form'], 7); // 'editer_article' -> 'article'
			$nom_table	= table_objet_sql($objet); // article -> spip_articles
			$tab_rubriques_exclues	= lire_config("lim_rubriques/$objet");

			if ($tab_rubriques_exclues) {
				$nbre_rubriques_autorisees = lim_nbre_rubriques_autorisees($objet);

				// Cas #0 : voir TODO's
				// if ($nbre_rubriques_autorisees == 0) {
				// 	debug('Cas #0');
				// 	$id_parent = '0';
				// }

				if ($nbre_rubriques_autorisees == 1) { // Cas #1.1
					$tab_rubrique_choisie = lim_publierdansrubriques($objet);
					$id_parent = implode($tab_rubrique_choisie);
				}

				if ($nbre_rubriques_autorisees >= 2) { // Cas #1.2
					$id_parent = '';
				}
			} else { // Cas #2
				// ici dans l'idéal, il faudrait utiliser l'API du plugin Declarer_parent
				$trouver_table = charger_fonction('trouver_table', 'base');
				$desc = $trouver_table($nom_table);
				if (isset($desc['field']['id_rubrique'])) {
					$id_parent = '';
				}
			}
		}

		if (isset($id_parent)) {
			$flux['data']['id_parent'] = $id_parent;
		}
	}

	return $flux;
}

/**
 * Gestion des contenus par rubrique :
 * Impossible de CREER ou DEPLACER un objet dans une rubrique interdite par la configuration choisie dans exec=configurer_lim_rubriques
 * exception : possibilité de modifier un objet si celui-ci est maintenant dans une rubrique où il est interdit de créer ce type d'objet.
 *
 * @param array $flux
 * @return array $flux
 *     le flux data complété ou non d'un message d'erreur
**/
function lim_formulaire_verifier($flux) {
	// si ce n'est pas un formulaire d'édition d'un objet ou si la restriction par rubrique n'a pas été activée, on sort.
	if (strncmp($flux['args']['form'], 'editer_', 7) !== 0 OR is_null(lire_config('lim/rubriques/objets'))) {
		return $flux;
	}

	$objet = substr($flux['args']['form'], 7); // 'editer_objet' devient 'objet'
	$nom_table	= table_objet_sql($objet);
	$tableau_tables_lim	= lire_config('lim/rubriques/objets');

	if (in_array($nom_table, $tableau_tables_lim)) {
		include_spip('inc/autoriser');

		$id_objet = $flux['args']['args'][0];
		if (is_numeric($id_objet)) { 	// c'est donc une modification,

			// récupérer l'id_rubrique actuel de l'objet
			// note : dans l'idéal, il faudrait utiliser le plugin déclarer parent ici
			$where = id_table_objet($objet).'='.$id_objet;
			$trouver_table = charger_fonction('trouver_table', 'base');
			$desc = $trouver_table($nom_table);
			if (isset($desc['field']['id_rubrique'])) {
				$id_rub_en_cours = sql_getfetsel('id_rubrique', $nom_table, $where);
			}

			// si c'est un déplacement vers une autre rubrique, on vérifie
			$faire = 'creer'.$objet.'dans';
			$id_parent = _request('id_parent');
			if (isset($id_rub_en_cours) and $id_parent != '0' and $id_rub_en_cours != $id_parent ) {
				if (!autoriser($faire, 'rubrique', _request('id_parent'))) {
					$flux['data']['id_parent'] = _T('lim:info_deplacer_dans_rubrique_non_autorise');
				}
			}
		}
		else { //c'est une création
			// en fait, cela ne sert à rien...snif...à cause de /echafaudage qui intercepte les créations avant le CVT (?!).
			// if (!autoriser($faire, 'rubrique', _request('id_parent'))) {
			// 	$flux['data']['id_parent'] = _T('lim:info_creer_dans_rubrique_non_autorise');
		}
	}

	return $flux;
}

/**
 * Gestion des contenus par rubrique :
 * masquer l'input id_parent si la config de l’objet renvoie une seule rubrique dans laquelle publier
 *
 * @param array $flux
 * @return array $flux
 */
function lim_formulaire_fond($flux) {
	// si ce n'est pas un formulaire d'édition d'un objet ou si la restriction par rubrique n'a pas été activée, on sort.
	if (strncmp($flux['args']['form'], 'editer_', 7) !== 0 OR is_null(lire_config('lim/rubriques/objets'))) {
		return $flux;
	}

	$objet = substr($flux['args']['form'], 7); // 'editer_objet' devient 'objet'
	if (lim_nbre_rubriques_autorisees($objet) <= 1){
		$flux['data'] .= "<style>.editer.editer_parent, .editer.editer_rubrique {display:none}</style>";
	}

	return $flux;
}

/**
 * Gestion de la desactivation de l'affichage de certain champs dans le formulaire Editer Auteur
 * Inserer le JS qui gére l'affichage ou non des champs dans certains formulaires historiques
 * juste le formulaire Auteur
 *
 * @param array $flux
 * @return array
 */
function lim_recuperer_fond($flux) {
	if ($flux['args']['fond'] == "formulaires/editer_auteur") {
		$ajout_script = recuperer_fond('prive/squelettes/inclure/lim');
		$flux['data']['texte'] = str_replace('</form>', '</form>'. $ajout_script, $flux['data']['texte']);
	}
	return $flux;
}

/**
 * Gestion des contenus par rubrique :
 * lors de la création d'une rubrique, mettre la config à la bonne valeur pour les objets "figé"
 * Autrement dit, désactiver la possibilité de publier les objets dits "figé"" dans cette nouvelle rubrique
 *
 * @param array $flux
 * @return array $flux
 */
function lim_post_insertion($flux) {
	if (isset($flux['args']['table']) 
		and $flux['args']['table'] == 'spip_rubriques'
	) {
		$id_rubrique = $flux['args']['id_objet'];
		include_spip('inc/config');
		if ($tables_objet = lire_config('lim/rubriques/cadenas')) {
			foreach ($tables_objet as $table) {
				$objet_type = objet_type($table);
				$valeurs = lire_config("lim_rubriques/$objet_type");
				if ($valeurs) {
					$valeurs[] = $id_rubrique;
					ecrire_config("lim_rubriques/$objet_type", $valeurs);
				}
			}
		}
	}

	return $flux;
}
