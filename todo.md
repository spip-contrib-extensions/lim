﻿## TODO


**1- Gestion des contenus par rubrique**
Pouvoir gérer le cas où un rédacteur décoche toutes les cases : du coup on ne retrouve l'objet éditorial que via  le menu Édition -> Objets

**2- Utiliser l'API Declarer Parent dans ecrire/base/objets**
Notament dans le pipeline lim_formulaire_verifier()
Voir https://core.spip.net/issues/3844

**3- Gestion des autorisations de création ou de déplacement**
Comment shunter la gestion des autorisations présente dans /prive/echafaudage/contenu/objet ?
Comment vérifier un déplacement depuis le bouton 'Déplacer' (voir /prive/echafaudage/hierarchie/objet) ?

**4- Revoir la gestion des metas de configuration**
Prendre exemple sur le plugin Rang
