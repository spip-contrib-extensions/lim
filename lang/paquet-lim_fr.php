<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'lim_description' => '&nbsp;<br>3 parties sont proposées pour allèger le back-office:

	- <b>Forums publics</b> : désactiver l\'affichage de ce bloc dans les pages articles,

	- <b>Divers</b> : désactiver l\'affichage de certains champs (Bio, PGP,etc.) dans le formulaire auteur,

	- <b>Logos</b> : si vous utiliser les logos, vous pourrez alors désactiver leur affichage au cas par cas (articles, auteurs, mots-clés, etc.),

	- <b>Contenus par rubriques</b> : Choisir les rubriques où désactiver l\'édition d\'articles, brèves, sites référencés et autres objets éditoriaux.',
	'lim_nom' => 'Lim',
	'lim_slogan' => 'Alléger l\'espace privé de blocs et boutons inutiles dans votre projet',
);